use clap::{App, Arg};
// use csv::{WriterBuilder};
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};

fn main() -> Result<(), Box<dyn Error>> {
    // Parse command-line arguments
    let matches = App::new("CSV to Lowercase")
        .version("1.0")
        .author("XZH-IDS721_W8")
        .about("Reads a CSV file and converts all text to lowercase and remove all special symbols")
        .arg(Arg::with_name("FILE_PATH")
            .help("Sets the input CSV file")
            .required(true)
            .index(1))
        .get_matches();

    // Retrieve the input file path from command-line arguments
    let input_file = matches.value_of("FILE_PATH").unwrap();
    println!("Processing file: {}", input_file);

    // Process the input file
    process_file(input_file)?;

    println!("Done!");
    Ok(())
}

// Process the input CSV file
fn process_file(path: &str) -> Result<(), Box<dyn Error>> {
    // Open the input file
    let file = File::open(path)?;

    // Create a buffered reader for efficient reading
    let reader = io::BufReader::new(file);

    // Create a CSV writer with space as delimiter and direct output to stdout
    let mut wtr = csv::WriterBuilder::new().delimiter(b' ').from_writer(io::stdout());

    // Iterate over each line in the input CSV file
    for line in reader.lines() {
        let record = line?;
        // Process each record and write to CSV
        let processed_record = process_record(&record);
        // Print the original text content
        println!("Original: {}", &record);
        println!("Processed: {:?}", &processed_record);
        // let processed_string = processed_record.join(" ").to_lowercase();
        // println!("Processed: {}", &processed_string);
        wtr.write_record(&processed_record)?;
    }

    Ok(())
}

// Process each record (line) of the CSV file
fn process_record(record: &str) -> Vec<String> {
    // Convert each character to lowercase if alphabetic, else replace with space
    record
        .chars()
        .filter(|&c| c.is_ascii_alphabetic() || c.is_ascii_whitespace())
        .map(|c| if c.is_ascii_alphabetic() { c.to_ascii_lowercase() } else { ' ' })
        .collect::<String>()
        .split_whitespace()
        .map(|s| s.to_string())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_record() {
        // Test case with alphabetic characters and spaces
        let record = "Hello World!";
        let expected = vec!["hello".to_string(), "world".to_string()];
        assert_eq!(process_record(record), expected);

        // Test case with alphabetic characters, spaces, and non-alphabetic characters
        let record = "Hello, World!";
        let expected = vec!["hello".to_string(), "world".to_string()];
        assert_eq!(process_record(record), expected);

        // Test case with only non-alphabetic characters
        let record = "!@#$%^&*()";
        let expected: Vec<String> = Vec::new();
        assert_eq!(process_record(record), expected);
    }
}
