# Rust Command-Line Tool with Testing

## Requirements
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Preparation
1. Set up Rust development environment

You can follow the guidance [here](https://www.rust-lang.org/learn/get-started) to prepare for the environment. Make sure you have these key components: **Rustup** (the Rust installer and version management tool) and **Cargo** ([the Rust build tool and package manager](https://doc.rust-lang.org/cargo/getting-started/first-steps.html))

```bash
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# For codespace you should mkdir first
# mkdir -p /home/codespace/.config/fish/conf.d/

# For MacOS and Linux
curl https://sh.rustup.rs -sSf | sh

# Configure the current shell
source $HOME/.cargo/env

# Start a new package with Cargo. Cargo defaults to --bin to make a binary program. To make a library, we would pass --lib, instead.
cargo new w8_rustcommandtool
```

An easier way to prepare for it is using the github codespace with a `.devcontainer` folder. Include the `Dockerfile`and `devcontainer.json` in it. However, you can not try to build a docker image on your own within the codespace run by `.devcontainer`. Maybe github codespace do not support this feature :(
```Dockerfile
FROM mcr.microsoft.com/devcontainers/rust:0-1-bullseye

# Include lld linker to improve build times either by using environment variable
# RUSTFLAGS="-C link-arg=-fuse-ld=lld" or with Cargo's configuration file (i.e see .cargo/config.toml).
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
   && apt-get -y install clang lld \
   && apt-get autoremove -y && apt-get clean -y
```

2. Add dependencies

You may edit the `Cargo.toml` as the following content.
```toml
[package]
name = "w8_rustcommandtool"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
clap = "3.0"
csv = "1.1"

``` 

3. Implementing Your Command-Line Tool
- Define your CLI interface and functionality in `main.rs`.
- Set up the input data in `data` folder.


## Main Funtionality
**1. Command Line Tool functionality**

It transforms all the text data in the CSV file to lowercase and remove all symbols except the letters.

**2. Data ingestion/processing**

The project incorporates the csv crate in Rust to enable the opening and reading of CSV files. This functionality is achieved using the ReaderBuilder from the csv crate, which simplifies the parsing of CSV data from a specified file.

The primary data processing task involves converting the content of the CSV data to lowercase and removing all special symbols. This processing occurs on a record-by-record basis, where each field within a record is transformed to uppercase.

![alt text](image-2.png)

**3. Testing implementation**
![alt text](image-3.png)

## Run the project
1. Navigate to your project directory in the terminal and run the project with Cargo.
```bash
cargo build --release

cargo run -- <FILE_PATH>
```
This compiles and executes your tool with the provided command-line arguments.

![alt text](image.png)

2. Test functionality
Use `cargo test` for unit testing.
```bash
cargo test

# If you have a makefile, you can run the test with the following command
make test
```
![alt text](image-1.png)
